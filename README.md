# MJs Sublime Text Settings #

These are my preferred settings for the text editor Sublime Text.

### To use ###

From Sublime Text go to 
```
#!html

Sublime Text -> Preferences -> Browse Packages...
```
 and copy the two included files in to the User folder. All plugins should install on restart.